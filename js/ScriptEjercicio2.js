const convertidor = document.getElementById('convertidor');
const limpiar = document.getElementById('limpiar');
const txtCantidad = document.getElementById('cantidad');
const resultado = document.getElementById('resultado');
const celsiusToFahrenheit = document.getElementById('c-f');
const fahrenheitToCelsius = document.getElementById('f-c');

convertidor.addEventListener('click', function() {
    const cantidad = parseFloat(txtCantidad.value);

    if (celsiusToFahrenheit.checked) {
        const fahrenheit = (cantidad * 9/5) + 32;
        resultado.value = fahrenheit.toFixed(2);
    } else if (fahrenheitToCelsius.checked) {
        const celsius = (cantidad - 32) * 5/9;
        resultado.value = celsius.toFixed(2);
     }
});

limpiar.addEventListener('click', function() {
    txtCantidad.value = '';
    resultado.value = '';
    celsiusToFahrenheit.checked = false;
    fahrenheitToCelsius.checked = false;
});
