function calcularCosto() {
    var precio = parseFloat(document.getElementById("precio").value);
    var tipoViaje = document.getElementById("tipoViaje").value;

    var subtotal = precio;
    var impuesto = subtotal * 0.16;

    if (tipoViaje === "doble") {
        subtotal *= 1.8;
    }
    var totalPagar = subtotal + impuesto;

    mostrarResultado("subtotal", subtotal);
    mostrarResultado("impuesto", impuesto);
    mostrarResultado("totalPagar", totalPagar);
}

function limpiarFormulario() {
    document.getElementById("formulario").reset();
    ocultarResultado("subtotal");
    ocultarResultado("impuesto");
    ocultarResultado("totalPagar");
}

function mostrarResultado(id, valor) {
    var elemento = document.getElementById(id);
    if (!elemento) {
        elemento = document.createElement("span");
        elemento.id = id;
        elemento.textContent = id + ": $" + valor.toFixed(2);
        document.querySelector("form").appendChild(elemento);
    } else {
        elemento.textContent = id + ": $" + valor.toFixed(2);
    }
}

function ocultarResultado(id) {
    var elemento = document.getElementById(id);
    if (elemento) {
        elemento.remove();
    }
}
